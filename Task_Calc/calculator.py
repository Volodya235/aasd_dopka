from typing import List
from oop.element import (Element, Operation, Summ, Subtr, Mult, Div, Pow,
                         OpenBracket, ClosedBracket, Number, extract, perform_calculation)


class Calculator:
    def _transform(self, input_: str) -> str:
        stack: List[Element] = []
        result = ""

        for value in input_.split():
            element = Element().create(value)

            if isinstance(element, Number):
                result = " ".join([result, str(element)])
            elif isinstance(element, Operation):
                while element.is_pop_needed(stack):
                    result = stack[len(stack) - 1].pop(result)
                    del stack[len(stack) - 1]

                if element.value != ')':
                    stack.append(element)

        result = extract(stack, result)
        return result

    def _calculate(self, notation: str) -> float:
        stack = []
        return perform_calculation(notation, stack)

    def calculate(self, input_: str) -> float:
        # переводит в польскую запись
        notation: str = self._transform(input_)
        # вычисляет результат польской записи
        result: float = self._calculate(notation)

        return result


# ENTER STRING WITH WHITESPACES BETWEEN NUMBERS AND OPERATIONS
# EXAMPLE: 2 * ( 4 + 10 ) / 4 - 1
operation = input()

calculator = Calculator()
result = calculator.calculate(operation)
print(result)
