import sys


class InputError(TypeError):
    pass


class Element:
    def create(self, str: str):
        if str.isdigit():
            return Number(str)
        elif str in ["+", "-", "*", "/", "(", ")", "^"]:
            return Operation().create(str)
        else:
            raise InputError("Invalid Symbol or Form of Typing")


class Operation(Element):
    def create(self, str: str):
        _operations = {
            "+": Summ,
            "-": Subtr,
            "*": Mult,
            "/": Div,
            "^": Pow,
            "(": OpenBracket,
            ")": ClosedBracket,
        }

        try:
            return _operations[str]()
        except KeyError:
            raise InputError("Invalid Symbol or Form of Typing")

    def __str__(self):
        return self.value  # type: ignore

    def __le__(self, operation):
        return self.priority <= operation.priority  # type: ignore

    def is_pop_needed(self, stack: list) -> bool:
        return (
            len(stack)  # type: ignore
            and self <= stack[len(stack) - 1]
            and stack[len(stack) - 1].value != "("
        )

    def pop(self, str_: str) -> str:
        if self.value != "(":  # type: ignore
            str_ = " ".join([str_, str(self)])

        return str_


class Summ(Operation):
    priority = 0
    value = "+"


class Subtr(Operation):
    priority = 0
    value = "-"


class Mult(Operation):
    priority = 1
    value = "*"


class Div(Operation):
    priority = 1
    value = "/"


class Pow(Operation):
    priority = 3
    value = "^"


class OpenBracket(Operation):
    priority = 2
    value = "("


class ClosedBracket(Operation):
    priority = -1
    value = ")"

    def is_pop_needed(self, stack: list):
        if stack[len(stack) - 1].value == "(":
            del stack[len(stack) - 1]
            return False
        else:
            return len(stack) and (self <= stack[len(stack) - 1])


class Number(Element):
    def __init__(self, str):
        self.value = str

    def __str__(self):
        return self.value


def extract(stack: list, result: str) -> str:
    for element in reversed(stack):
        result = " ".join([result, str(element)])

    return result


def perform_calculation(notation: str, stack: list) -> float:
    for element in notation.split():
        if element == "+":
            stack[len(stack) - 2] = (
                stack[len(stack) - 2] + stack[len(stack) - 1]
            )
            del stack[len(stack) - 1]
        elif element == "-":
            stack[len(stack) - 2] = (
                stack[len(stack) - 2] - stack[len(stack) - 1]
            )
            del stack[len(stack) - 1]
        elif element == "*":
            stack[len(stack) - 2] = (
                stack[len(stack) - 2] * stack[len(stack) - 1]
            )
            del stack[len(stack) - 1]
        elif element == "/":
            try:
                stack[len(stack) - 2] = (
                    stack[len(stack) - 2] / stack[len(stack) - 1]
                )
                del stack[len(stack) - 1]
            except ZeroDivisionError:
                print("Division by zero")
                sys.exit()

        elif element == "^":
            stack[len(stack) - 2] = (
                stack[len(stack) - 2] ** stack[len(stack) - 1]
            )
            del stack[len(stack) - 1]
        else:
            stack.append(int(element))

    return stack[0]
