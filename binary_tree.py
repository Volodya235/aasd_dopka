class Node:
    def __init__(self, val_):
        self.left = None
        self.right = None
        self.val = val_


def insert(root, val_):
    if root is None:
        return Node(val_)
    else:
        if root.val == val_:
            return root
        elif root.val < val_:
            root.right = insert(root.right, val_)
        else:
            root.left = insert(root.left, val_)
    return root


def inorder_tree(root):
    if root:
        inorder_tree(root.left)
        print(root.val)
        inorder_tree(root.right)


def construct_tree_from_list(list_):
    root = Node(list_[0])
    for elem in list_[1:]:
        root = insert(root, elem)

    return root


# x = [0, 3, 2, 1, 4, 5]
# r = construct_tree_from_list(x)
# inorder_tree(r)
