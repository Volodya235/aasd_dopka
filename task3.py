import sys

from binary_tree import Node, insert, construct_tree_from_list


stack = []


def traversal(node, num):
    stack.append(node.val)
    if node.val == num:
        print(*stack)
        sys.exit()
    else:
        traversal(node.left, num) if node.left else None
        traversal(node.right, num) if node.right else None
        stack.pop()


tree = list(map(int, input('Enter tree like a sequence with whitespaces: ').split()))
x = int(input('Enter number: '))
root = construct_tree_from_list(tree)
traversal(root, x)

# На вход подаётся бинарное дерево поиска и какое-то число X. Нужно найти путь из корневого узла до
# узла, содержащего Х. Если Х нет в дереве, вывести сообщение об этом.

